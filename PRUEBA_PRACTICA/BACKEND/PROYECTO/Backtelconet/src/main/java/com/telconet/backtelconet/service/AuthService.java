package com.telconet.backtelconet.service;

import com.telconet.backtelconet.models.Usuario;
import com.telconet.backtelconet.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    public boolean autenticar(String correo, String password) {
        Usuario usuario = usuarioRepository.findByUsuario(correo);
        if (usuario != null) {
            return usuario.getPassword().equals(password);
        }
        return false;
    }
}
