package com.telconet.backtelconet.config;

public class Credenciales {
    private String correo;
    private String password;

    // Constructor
    public Credenciales(String correo, String password) {
        this.correo = correo;
        this.password = password;
    }

    // Getters y setters
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
