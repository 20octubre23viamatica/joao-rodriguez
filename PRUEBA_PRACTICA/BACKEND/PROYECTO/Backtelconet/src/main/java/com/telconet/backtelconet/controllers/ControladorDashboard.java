package com.telconet.backtelconet.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

@Controller
public class ControladorDashboard {
    @GetMapping("/dashboard")
    public String mostrarDashboard(HttpSession session) {
        // Obtener el ID de usuario de la sesión
        Long userId = (Long) session.getAttribute("userId");

        if (userId != null) {
            // El usuario está autenticado y puede acceder al dashboard
            // Realiza las operaciones del dashboard aquí
            return "dashboard";
        } else {
            // Si el usuario no está autenticado, redirige al inicio de sesión
            return "redirect:/login";
        }
    }
}
