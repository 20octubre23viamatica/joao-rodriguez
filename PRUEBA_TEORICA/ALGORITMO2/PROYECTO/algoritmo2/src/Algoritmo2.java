/**
 * Clase Algoritmo2
 *
 * Se tiene una X en la esquina superior izquierda de un área de 4x4. Se tiene una matriz donde se podrá definir una
 * cantidad ilimitada de elementos. Cada 2 elementos de la matriz corresponden a un movimiento, el primero en el eje
 * horizontal y el segundo en el eje vertical. El número indica las unidades a moverse y el signo la dirección
 * (positivo para derecha o abajo, negativo para izquierda o arriba)
 */
public class Algoritmo2 {
    public static void main(String[] args) {
        char[][] area = inicializarArea(); // Inicializa un área de 4x4 con 'O' en todas partes
        int posX = 0; // Posición inicial en el eje horizontal
        int posY = 0; // Posición inicial en el eje vertical
        int[] myArray = {1, 2, -1, 1, 0, 1, 2, -1, -1, -2}; // Arreglo de instrucciones para mover la "X"

        moverX(area, posX, posY, myArray); // Mueve la "X" según las instrucciones
        imprimirArea(area); // Imprime el área con la posición final de la "X"
    }

    /**
     * Inicializa un área de 4x4 con 'O' en todas partes y retorna el área.
     *
     * @return El área inicializada.
     */
    public static char[][] inicializarArea() {
        char[][] area = new char[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                area[i][j] = 'O';
            }
        }
        return area;
    }

    /**
     * Mueve la "X" en el área según las instrucciones en myArray.
     *
     * @param area El área en la que se mueve la "X".
     * @param posX La posición inicial de la "X" en el eje horizontal.
     * @param posY La posición inicial de la "X" en el eje vertical.
     * @param myArray El arreglo de instrucciones para mover la "X".
     */
    public static void moverX(char[][] area, int posX, int posY, int[] myArray) {
        for (int i = 0; i < myArray.length; i += 2) {
            int movX = myArray[i];
            int movY = myArray[i + 1];
            if (posX + movX >= 0 && posX + movX < 4 && posY + movY >= 0 && posY + movY < 4) {
                area[posY][posX] = 'O'; // Coloca "O" en la posición anterior
                posX += movX;
                posY += movY;
                area[posY][posX] = 'X'; // Coloca "X" en la nueva posición
            }
        }
    }

    /**
     * Imprime el área con la posición final de la "X".
     *
     * @param area El área a imprimir.
     */
    public static void imprimirArea(char[][] area) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(area[i][j]);
            }
            System.out.println();
        }
    }
}
