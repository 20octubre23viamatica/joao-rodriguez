BEGIN;


CREATE TABLE IF NOT EXISTS public.admi_rol
(
    id integer NOT NULL DEFAULT nextval('admi_rol_id_seq'::regclass),
    nombre character varying(255) COLLATE pg_catalog."default" NOT NULL,
    descripcion text COLLATE pg_catalog."default",
    CONSTRAINT admi_rol_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.admi_usuario
(
    id integer NOT NULL DEFAULT nextval('admi_usuario_id_seq'::regclass),
    usuario character varying(255) COLLATE pg_catalog."default" NOT NULL,
    password character varying(255) COLLATE pg_catalog."default" NOT NULL,
    nombres character varying(255) COLLATE pg_catalog."default" NOT NULL,
    apellidos character varying(255) COLLATE pg_catalog."default" NOT NULL,
    direccion text COLLATE pg_catalog."default",
    telefono character varying(20) COLLATE pg_catalog."default",
    usuario_creacion character varying(255) COLLATE pg_catalog."default",
    fecha_creacion timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    foto bytea,
    CONSTRAINT admi_usuario_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.info_usuario_rol
(
    id integer NOT NULL DEFAULT nextval('info_usuario_rol_id_seq'::regclass),
    usuario_id integer,
    rol_id integer,
    usuario_creacion character varying(255) COLLATE pg_catalog."default",
    fecha_creacion timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    usuario_modificacion character varying(255) COLLATE pg_catalog."default",
    fecha_modificacion timestamp with time zone,
    estado boolean,
    CONSTRAINT info_usuario_rol_pkey PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.info_usuario_rol
    ADD CONSTRAINT fk_usuario_rol_rol FOREIGN KEY (rol_id)
    REFERENCES public.admi_rol (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;


ALTER TABLE IF EXISTS public.info_usuario_rol
    ADD CONSTRAINT fk_usuario_rol_usuario FOREIGN KEY (usuario_id)
    REFERENCES public.admi_usuario (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;


ALTER TABLE IF EXISTS public.info_usuario_rol
    ADD CONSTRAINT info_usuario_rol_rol_id_fkey FOREIGN KEY (rol_id)
    REFERENCES public.admi_rol (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;


ALTER TABLE IF EXISTS public.info_usuario_rol
    ADD CONSTRAINT info_usuario_rol_usuario_id_fkey FOREIGN KEY (usuario_id)
    REFERENCES public.admi_usuario (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

END;