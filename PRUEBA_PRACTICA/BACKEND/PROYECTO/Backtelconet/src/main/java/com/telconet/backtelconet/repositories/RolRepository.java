package com.telconet.backtelconet.repositories;

import com.telconet.backtelconet.models.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolRepository extends JpaRepository<Rol, Long> {
    Rol findByNombre(String nombre);

    List<Rol> findByDescripcionContaining(String descripcion);

    List<Rol> findAllByOrderByNombreAsc();

    long count();

    void deleteByNombre(String nombre);

    boolean existsByNombre(String nombre);
}