package com.telconet.backtelconet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BacktelconetApplication {

	public static void main(String[] args) {
		SpringApplication.run(BacktelconetApplication.class, args);
	}

}
