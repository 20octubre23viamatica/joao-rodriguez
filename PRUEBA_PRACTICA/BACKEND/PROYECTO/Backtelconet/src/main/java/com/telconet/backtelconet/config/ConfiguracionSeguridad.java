package com.telconet.backtelconet.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class ConfiguracionSeguridad extends WebSecurityConfigurerAdapter {


    // Configuración de las reglas de seguridad
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/login").permitAll() // Página de inicio de sesión accesible para todos
                .antMatchers("/dashboard").authenticated() // Página de dashboard protegida
                .and()
                .formLogin()
                .loginPage("/login") // Página de inicio de sesión personalizada
                .successForwardUrl("/dashboard") // Redirige al dashboard después del inicio de sesión
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) // Ruta para cerrar sesión
                .logoutSuccessUrl("/login") // Redirige a la página de inicio de sesión después del cierre de sesión
                .and()
                .csrf().disable(); // Desactivar CSRF para simplificar el ejemplo
    }

    // Configuración del encriptador de contraseñas
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
