package com.telconet.backtelconet.config;

import com.telconet.backtelconet.models.Rol;
import com.telconet.backtelconet.models.Usuario;
import com.telconet.backtelconet.repositories.RolRepository;
import com.telconet.backtelconet.repositories.UsuarioRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DataInitializer implements CommandLineRunner {

    // Inyecta los repositorios necesarios
    private final RolRepository rolRepository;
    private final UsuarioRepository usuarioRepository;

    public DataInitializer(RolRepository rolRepository, UsuarioRepository usuarioRepository) {
        this.rolRepository = rolRepository;
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        // Lógica para crear el rol ADMINISTRADOR
        Rol administrador = rolRepository.findByNombre("ADMINISTRADOR");
        if (administrador == null) {
            administrador = new Rol();
            administrador.setNombre("ADMINISTRADOR");
            administrador.setDescripcion("Rol de administrador");
            rolRepository.save(administrador);
        }

        // Lógica para crear el usuario administrador
        Usuario adminUsuario = usuarioRepository.findByUsuario("administrador@prueba.com");
        if (adminUsuario == null) {
            adminUsuario = new Usuario();
            adminUsuario.setUsuario("administrador@prueba.com");
            // Establece otros atributos del usuario (contraseña, nombres, apellidos, etc.)
            adminUsuario.setPassword("telconet");
            adminUsuario.setNombres("Admin");
            adminUsuario.setApellidos("Admin");

            // Establece otros atributos del usuario
            usuarioRepository.save(adminUsuario);
        }
    }
}

