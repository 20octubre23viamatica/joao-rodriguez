package com.telconet.backtelconet.service;

import com.telconet.backtelconet.models.Usuario;
import com.telconet.backtelconet.repositories.UsuarioRepository;

public class UsuarioServicio {
    private final UsuarioRepository usuarioRepositorio;

    public UsuarioServicio(UsuarioRepository usuarioRepositorio) {
        this.usuarioRepositorio = usuarioRepositorio;
    }

    public Usuario buscarPorCorreo(String correo) {
        return usuarioRepositorio.findByUsuario(correo);
    }
}
