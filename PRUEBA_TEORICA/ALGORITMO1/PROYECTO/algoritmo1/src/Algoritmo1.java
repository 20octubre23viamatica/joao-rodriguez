import java.util.ArrayList;
import java.util.List;

/**
 * Clase Algoritmo1
 *
 * Tiene un arreglo (llamado int[] myArray) con 10 elementos (que solo acepte números enteros en el rango de 1 al 9).
 * Escriba un programa en Java que imprima el número que tiene más ocurrencias seguidas en el arreglo y también imprimir la cantidad de veces que aparece
 */
public class Algoritmo1 {
    /**
     * El método principal del programa.
     *
     * @param args Los argumentos de la línea de comandos (no se utilizan en este programa).
     */
    public static void main(String[] args) {
        // Arreglo de entrada con valores de ejemplo
        int[] miArray = {1, 2, 2,2, 5, 4,4,4, 6, 7, 8, 8, 8};
        int[] myArray2 = { 1, 2, 2, 5, 4, 6, 7, 8, 8, 8 };

        // Llama a la función encontrarSecuenciasMasLargas para obtener el resultado
        List<int[]> resultados = encontrarSecuenciasMasLargas(miArray);

        // llama al metodo imprimir
        imprimirResultados(resultados);
    }

    /**
     * Encuentra secuencias de números con la misma ocurrencia máxima en un arreglo de enteros.
     *
     * @param arr El arreglo de enteros en el que se busca secuencias.
     * @return Una lista de arreglos, donde cada arreglo contiene el número de recurrencias y el número correspondiente de la secuencia más larga.
     */
    public static List<int[]> encontrarSecuenciasMasLargas(int[] arr) {
        // Lista para almacenar los resultados de las secuencias más largas
        List<int[]> resultados = new ArrayList<>();

        int contadorActual = 1;          // Contador de recurrencias del número actual
        int contadorMaximo = 1;         // Contador de recurrencias de la secuencia más larga encontrada

        // Recorre el arreglo a partir del segundo elemento (índice 1)
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == arr[i - 1]) {
                // Si el elemento actual es igual al anterior, aumenta el contador actual
                contadorActual++;
                if (contadorActual == contadorMaximo) {
                    // Si el contador actual es igual al contador máximo, agrega la secuencia a los resultados
                    resultados.add(new int[]{contadorActual, arr[i]});
                } else if (contadorActual > contadorMaximo) {
                    // Si el contador actual es mayor que el contador máximo, borra los resultados anteriores y agrega la nueva secuencia
                    resultados.clear();
                    resultados.add(new int[]{contadorActual, arr[i]});
                    contadorMaximo = contadorActual;
                }
            } else {
                // Si el elemento actual es diferente al anterior, restablece el contador actual a 1
                contadorActual = 1;
            }
        }

        return resultados;
    }
    /**
     * Imprime los resultados de las secuencias con la misma ocurrencia máxima.
     *
     * @param resultados La lista de resultados a imprimir.
     */
    public static void imprimirResultados(List<int[]> resultados) {
        for (int[] resultado : resultados) {
            System.out.println("Recurrencias: " + resultado[0]);
            System.out.println("Número: " + resultado[1]);
        }
    }

}
