-- Database: prueba_telconet

DROP DATABASE IF EXISTS prueba_telconet;

CREATE DATABASE prueba_telconet
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Spain.1252'
    LC_CTYPE = 'Spanish_Spain.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;