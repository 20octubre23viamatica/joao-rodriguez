package com.telconet.backtelconet.controllers;

import com.telconet.backtelconet.config.Credenciales;
import com.telconet.backtelconet.models.Usuario;
import com.telconet.backtelconet.service.AuthService;
import com.telconet.backtelconet.service.UsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class ControladorInicioSesion {
    @Autowired
    private AuthService authService;
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/api/login")
    public ResponseEntity<String> iniciarSesion(@RequestBody Credenciales credenciales) {
        if (authService.autenticar(credenciales.getCorreo(), credenciales.getPassword())) {
            // Autenticación exitosa
            return new ResponseEntity<>("Autenticación exitosa", HttpStatus.OK);
        } else {
            // Autenticación fallida
            return new ResponseEntity<>("Autenticación fallida", HttpStatus.UNAUTHORIZED);
        }
    }
    @GetMapping("/logout")
    public String cerrarSesion(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate(); // Invalida la sesión
        }
        return "redirect:/login"; // Redirige al inicio de sesión
    }

}
