package com.telconet.backtelconet.models;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "info_usuario_rol")
public class InfoUsuarioRol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "rol_id")
    private Rol rol;

    private String usuarioCreacion;
    private LocalDateTime fechaCreacion;
    private String usuarioModificacion;
    private LocalDateTime fechaModificacion;
    private boolean estado;

    // Getters y setters
}
